# MussumAcademy Backend

O backend da MussumAcademy é desenvolvido utilizando NestJS, NestJS Query, GraphQL e Docker para fornecer informações para o frontend da aplicação.

## Inicialização

Siga estas instruções para iniciar o backend localmente.

### Pré-requisitos

- Docker
- Docker Compose
- Node.js (versão 12 ou superior)

### Passos para inicialização

1. Certifique-se de que o Docker está instalado e o Docker Desktop está em execução.

2. Clone o repositório do projeto:
   ```bash
   git clone https://gitlab.com/vmoraes424/mussumacademy.git
   cd mussumacademy
   ```

3. Execute o Docker Compose para iniciar os serviços:
   ```bash
   docker-compose up -d
   ```

4. Aguarde até que os containers estejam em execução.

5. Inicie o servidor NestJS:
   ```bash
   npm run start
   ```

6. O servidor NestJS estará acessível em `http://localhost:3001`.

## Contexto Geral

O backend da MussumAcademy foi desenvolvido utilizando as seguintes tecnologias:

- **NestJS**: Framework Node.js para construção de aplicativos escaláveis e eficientes.
- **NestJS Query**: Plugin para simplificar a implementação de uma API GraphQL.
- **GraphQL**: Linguagem de consulta e manipulação de dados utilizada para fornecer informações ao frontend.
- **Docker**: Plataforma de contêineres que simplifica a implantação e execução de aplicativos.

O backend fornece os dados necessários para o frontend da MussumAcademy e é acessível através do servidor NestJS. Certifique-se de que o Docker está em execução antes de iniciar o backend.