import { InputType } from '@nestjs/graphql';

@InputType()
export class CreateDisciplineInput {
  nome: string;
}
