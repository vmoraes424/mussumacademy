import { InputType, Int, Field } from '@nestjs/graphql';
import { UpdateLessonInput } from 'src/module/lessons/dto/update-lesson.input';

@InputType()
export class CreateContentInput {
  description: string;
  linkContent?: string;
  lessonId?: string;
}
