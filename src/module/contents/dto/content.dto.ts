import { FilterableField, FilterableRelation } from '@nestjs-query/query-graphql';
import { Field, ObjectType } from '@nestjs/graphql';
import { BaseDTO } from 'src/module/bases/dto/base.dto';
import { LessonDTO } from 'src/module/lessons/dto/lesson.dto';

@ObjectType("Content")
@FilterableRelation('lesson', () => LessonDTO)
export class ContentDTO extends BaseDTO {
  @FilterableField({ nullable: true })
  description: string;
  @FilterableField({ nullable: true })
  linkContent: string;

}
